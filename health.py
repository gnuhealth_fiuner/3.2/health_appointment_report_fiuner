# -*- coding: utf-8 -*-
##############################################################################
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from datetime import datetime, time

from trytond.model import ModelView, ModelSQL, fields
from trytond.pool import PoolMeta,Pool
from trytond.transaction import Transaction

__all__ = [ 'HealthProfessional',
           'PatientData']


class PatientData(metaclass=PoolMeta):
    'Patient'
    __name__ = 'gnuhealth.patient'
    
    last_appointment = fields.Function(
        fields.DateTime('Ultima cita'),'get_last_appointment',
        searcher='search_last_appointment')
    
    def get_last_appointment(self,name):
        pool = Pool()        
        Appointment = pool.get('gnuhealth.appointment')
        Date = pool.get('ir.date')
        dt_today = datetime.combine(Date.today(),time())
        appointment = Appointment.search([
                            ('patient','=',self.id),
                            ('state','in',['checked_in','confirmed','done']),
                            ('appointment_date','<=',dt_today)
                            ])
        if appointment:
            return appointment[-1].appointment_date
        return None
    
    @classmethod
    def search_last_appointment(cls, name, clause):
        transaction = Transaction()
        connection = transaction.connection
        cursor = connection.cursor()
        _, operator,operand1  = clause

        pool = Pool()
        Appointment = pool.get('gnuhealth.appointment')
        Patient = pool.get('gnuhealth.patient')
        Date = pool.get('ir.date')

        appointment = Appointment.__table__()
        patient = Patient.__table__()

        result1 = []
        if operator:
            Operator = fields.SQL_OPERATORS[operator]            
            query1 = appointment.select(appointment.patient,
                    where= ((Operator(appointment.appointment_date,operand1))&
                            (appointment.patient != None)))
            cursor.execute(*query1)
            result1 = cursor.fetchall()
            result1 = list(set(result1))
        return [('id','in',result1)]


class HealthProfessional(metaclass=PoolMeta):
    'Health Proffessional'
    __name__ = 'gnuhealth.healthprofessional'    

    is_doctor = fields.Boolean(u"Es un médico?", 
                               help=u"Tildar si es un médico")
